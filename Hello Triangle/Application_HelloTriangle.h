#pragma once

#define GLFW_INCLUDE_VULKAN

#ifdef _DEBUG
	#define USE_VULKAN_VALIDATION_LAYER
#endif //_DEBUG

#include <vector>

#include <GLFW/glfw3.h>

class Application_HelloTriangle{
	private:
		struct QueueFamily_Indices{
			int family_graphics = -1;
			int family_presentation = -1;

			bool isComplete(){
				return (family_graphics >= 0) && (family_presentation >= 0);
			}
		};
		
		struct Details_SwapChain_Support{
			VkSurfaceCapabilitiesKHR surfaceCapabilities;
			std::vector<VkSurfaceFormatKHR> surfaceFormats;
			std::vector<VkPresentModeKHR> presentationModes;
		};
		
		const char* _APPLICATION_NAME;
		const std::vector<const char*> deviceExtensions = {
			VK_KHR_SWAPCHAIN_EXTENSION_NAME
		};
		unsigned int window_width;
		unsigned int window_height;
		GLFWwindow* _window;
		VkInstance instance;
		VkSurfaceKHR surface;
		VkPhysicalDevice device_physical = VK_NULL_HANDLE;
		VkDevice device_logical;
		VkQueue queue_graphics;
		VkQueue queue_presentation;
		VkSwapchainKHR swapChain = VK_NULL_HANDLE;
		VkFormat swapChain_imageFormat;
		VkExtent2D swapChain_extent;
		std::vector<VkImage> swapChain_images;
		std::vector<VkImageView> swapChain_imageViews;
		VkRenderPass renderPass;
		VkPipelineLayout pipeline_layout;
		VkPipeline pipeline_graphics;
		std::vector<VkFramebuffer> swapChain_frameBuffers;
		VkCommandPool command_pool;
		std::vector<VkCommandBuffer> command_buffers;
		VkSemaphore semaphore_imageAvailable;
		VkSemaphore semaphore_renderComplete;

		#ifdef USE_VULKAN_VALIDATION_LAYER
			const std::vector<const char*> validationLayers = {
				"VK_LAYER_LUNARG_standard_validation"
			};
			VkDebugReportCallbackEXT handle_callback_debugReport;
		#endif //USE_VULKAN_VALIDATION_LAYER
	public:
		Application_HelloTriangle(
			const unsigned int window_width,
			const unsigned int window_height,
			const char* _application_name
		);

		void run();
	private:
		static void onWindowResize(
			GLFWwindow* _window,
			const int width,
			const int height
		);

		//Stages
		void initializeWindow();
		void initializeVulkan();
		void draw();
		void cleanup();

		//Substages
		void createInstance();
		std::vector<const char*> getRequiredExtensions();
		void createSurface();
		void selectSuitablePhysicalDevice();
		bool isPhysicalDeviceSuitable(VkPhysicalDevice physicalDevice);
		bool checkDeviceExtensionSupport(VkPhysicalDevice physicalDevice);
		QueueFamily_Indices getQueueFamilies(VkPhysicalDevice physicalDevice);
		void createLogicalDevice();
		Details_SwapChain_Support querySwapChainSupport(VkPhysicalDevice physicalDevice);
		VkSurfaceFormatKHR selectSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats);
		VkPresentModeKHR selectSwapPresentationMode(const std::vector<VkPresentModeKHR> availablePresentationModes);
		VkExtent2D selectSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities);
		void createSwapChain();
		void createImageViews();
		VkShaderModule createShaderModule(const std::vector<char>& shader_code);
		void createRenderPass();
		void createGraphicsPipeline();
		void createFrameBuffers();
		void createCommandPool();
		void createCommandBuffers();
		void createSemaphores();
		void cleanupSwapChain();
		void recreateSwapChain();

		#ifdef USE_VULKAN_VALIDATION_LAYER
			bool checkValidationLayerSupport();
			void initializeDebugCallback();
			static VKAPI_ATTR VkBool32 VKAPI_CALL callback_debugReport(
				VkDebugReportFlagsEXT flags,
				VkDebugReportObjectTypeEXT objectType,
				uint64_t object,
				size_t location,
				int32_t code,
				const char* _layerPrefix,
				const char* _message,
				void* _customData
			);
		#endif //USE_VULKAN_VALIDATION_LAYER
};
