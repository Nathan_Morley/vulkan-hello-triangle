#include "Application_HelloTriangle.h"

#include <algorithm>
#include <fstream>
#include <iostream>
#include <set>

#ifdef USE_VULKAN_VALIDATION_LAYER
	/*
	Look up the address of the "vkCreateDebugReportCallbackEXT" function and return the function if successful
	*/
	VkResult CreateDebugReportCallbackEXT(
		VkInstance instance,
		const VkDebugReportCallbackCreateInfoEXT* _creationInformation,
		VkDebugReportCallbackEXT* _callback,
		const VkAllocationCallbacks* _allocator
	){
		VkResult result = VK_ERROR_EXTENSION_NOT_PRESENT;
		PFN_vkCreateDebugReportCallbackEXT function = (PFN_vkCreateDebugReportCallbackEXT)vkGetInstanceProcAddr(
			instance,
			"vkCreateDebugReportCallbackEXT"
		);

		if(function != nullptr){
			result = function(instance, _creationInformation, _allocator, _callback);
		}

		return result;
	}

	void DestroyDebugReportCallbackEXT(
		VkInstance instance,
		VkDebugReportCallbackEXT callback,
		const VkAllocationCallbacks* _allocator
	){
		PFN_vkDestroyDebugReportCallbackEXT function = (PFN_vkDestroyDebugReportCallbackEXT)vkGetInstanceProcAddr(
			instance,
			"vkDestroyDebugReportCallbackEXT"
		);

		if(function != nullptr){
			function(instance, callback, _allocator);
		} else{
			throw std::runtime_error("The vkDestroyDebugReportCallbackEXT extension is not present. If the vkCreateDebugReportCallbackEXT is present then this error should never throw.");
		}
	}
#endif //USE_VULKAN_VALIDATION_LAYER

#pragma region Helper Functions
	static std::vector<char> readFile(const std::string& file_name){
		std::ifstream file(
			file_name,
			std::ios::ate | std::ios::binary
		);

		if(!file.is_open()){
			throw std::runtime_error("Failed to open file.");
		}

		size_t file_size = (size_t)file.tellg();
		std::vector<char> file_buffer(file_size);

		file.seekg(0);
		file.read(
			file_buffer.data(),
			file_size
		);
		file.close();

		return file_buffer;
	}
#pragma endregion

Application_HelloTriangle::Application_HelloTriangle(
	const unsigned int window_width,
	const unsigned int window_height,
	const char* _application_name
):
	window_width(window_width),
	window_height(window_height),
	_APPLICATION_NAME(_application_name)
{}

void Application_HelloTriangle::onWindowResize(
	GLFWwindow* _window,
	const int width,
	const int height
){
	if(width != 0 && height !=0){
		Application_HelloTriangle* application = reinterpret_cast<Application_HelloTriangle*>(glfwGetWindowUserPointer(_window));
		application -> window_width = width;
		application -> window_height = height;

		application -> recreateSwapChain();
	}
}

void Application_HelloTriangle::run(){
	initializeWindow();
	initializeVulkan();

	while(!glfwWindowShouldClose(_window)){
		glfwPollEvents();
		draw();
	}

	vkDeviceWaitIdle(device_logical);

	cleanup();
}

void Application_HelloTriangle::initializeWindow(){
	glfwInit();
	glfwWindowHint(
		GLFW_CLIENT_API,
		GLFW_NO_API
	);

	_window = glfwCreateWindow(
		window_width, 
		window_height,
		_APPLICATION_NAME,
		nullptr,
		nullptr
	);

	glfwSetWindowUserPointer(
		_window,
		this
	);
	glfwSetFramebufferSizeCallback(
		_window,
		Application_HelloTriangle::onWindowResize
	);
}

void Application_HelloTriangle::initializeVulkan(){
	#ifdef USE_VULKAN_VALIDATION_LAYER
		std::cout << "Validation layer is enabled in debug builds." << std::endl << std::endl;

		if(checkValidationLayerSupport()){
	#endif //USE_VULKAN_VALIDATION_LAYER

	createInstance();

	#ifdef USE_VULKAN_VALIDATION_LAYER
		initializeDebugCallback();
	#endif //USE_VULKAN_VALIDATION_LAYER

	createSurface();
	selectSuitablePhysicalDevice();
	createLogicalDevice();
	createSwapChain();
	createImageViews();
	createRenderPass();
	createGraphicsPipeline();
	createFrameBuffers();
	createCommandPool();
	createCommandBuffers();
	createSemaphores();

	#ifdef USE_VULKAN_VALIDATION_LAYER
		}else{
			throw std::runtime_error("Validation layers requested are not available.");
		}
	#endif //USE_VULKAN_VALIDATION_LAYER
}

void Application_HelloTriangle::draw(){
	vkQueueWaitIdle(queue_presentation);

	uint32_t index_image;

	VkResult result_acquireNextImage = vkAcquireNextImageKHR(
		device_logical,
		swapChain,
		std::numeric_limits<uint64_t>::max(),
		semaphore_imageAvailable,
		VK_NULL_HANDLE,
		&index_image
	);

	if(result_acquireNextImage == VK_ERROR_OUT_OF_DATE_KHR){
		recreateSwapChain();
	}else if(result_acquireNextImage != VK_SUCCESS && result_acquireNextImage != VK_SUBOPTIMAL_KHR){
		throw std::runtime_error("Failed to acquire swap chain image.");
	}else{
		VkSubmitInfo information_submit_queue_graphics = {};

		information_submit_queue_graphics.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

		VkSemaphore wait_semaphores[] = {
			semaphore_imageAvailable
		};

		VkPipelineStageFlags wait_pipeline_stage_flags[] = {
			VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT
		};

		information_submit_queue_graphics.waitSemaphoreCount = 1;
		information_submit_queue_graphics.pWaitSemaphores = wait_semaphores;
		information_submit_queue_graphics.pWaitDstStageMask = wait_pipeline_stage_flags;
		information_submit_queue_graphics.commandBufferCount = 1;
		information_submit_queue_graphics.pCommandBuffers = &command_buffers.at(index_image);

		VkSemaphore signal_semaphores[] = {
			semaphore_renderComplete
		};

		information_submit_queue_graphics.signalSemaphoreCount = 1;
		information_submit_queue_graphics.pSignalSemaphores = signal_semaphores;

		if(
			vkQueueSubmit(
				queue_graphics,
				1,
				&information_submit_queue_graphics,
				VK_NULL_HANDLE
			) != VK_SUCCESS
		){
			throw std::runtime_error("Failed to submit draw command buffer.");
		}

		VkPresentInfoKHR information_presentation = {};

		information_presentation.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
		information_presentation.waitSemaphoreCount = 1;
		information_presentation.pWaitSemaphores = signal_semaphores;

		VkSwapchainKHR swapChains[] = {
			swapChain
		};

		information_presentation.swapchainCount = 1;
		information_presentation.pSwapchains = swapChains;
		information_presentation.pImageIndices = &index_image;
		information_presentation.pResults = nullptr;

		VkResult result_queuePresent = vkQueuePresentKHR(
			queue_presentation,
			&information_presentation
		);

		if(result_queuePresent == VK_ERROR_OUT_OF_DATE_KHR || result_queuePresent == VK_SUBOPTIMAL_KHR){
			recreateSwapChain();
		}else if(result_queuePresent != VK_SUCCESS){
			throw std::runtime_error("Failed to present swap chain image.");
		}
	}
}

void Application_HelloTriangle::cleanup(){
	//Vulkan cleanup
	cleanupSwapChain();
	vkDestroySemaphore(
		device_logical,
		semaphore_imageAvailable,
		nullptr
	);
	vkDestroySemaphore(
		device_logical,
		semaphore_renderComplete,
		nullptr
	);
	vkDestroyCommandPool(
		device_logical,
		command_pool,
		nullptr
	); //Destroying the command pool implicitly frees the command buffers that belong to the pool.
	vkDestroyDevice(
		device_logical,
		nullptr
	);

	#ifdef USE_VULKAN_VALIDATION_LAYER
		DestroyDebugReportCallbackEXT(
			instance,
			handle_callback_debugReport,
			nullptr
		);
	#endif //USE_VULKAN_VALIDATION_LAYER

	vkDestroySurfaceKHR(
		instance,
		surface,
		nullptr
	);
	vkDestroyInstance(
		instance, 
		nullptr
	);
	
	//GLFW cleanup
	glfwDestroyWindow(_window);
	glfwTerminate();
}

void Application_HelloTriangle::createInstance(){
	VkApplicationInfo information_application = {};

	information_application.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO; //Struct type
	information_application.pApplicationName = _APPLICATION_NAME; //Application name
	information_application.applicationVersion = VK_MAKE_VERSION(1, 0, 0); //Application version
	information_application.pEngineName = "No Engine"; //Engine name
	information_application.engineVersion = VK_MAKE_VERSION(1, 0, 0); //Engine version
	information_application.apiVersion = VK_API_VERSION_1_0; //Vulkan API version

	VkInstanceCreateInfo information_instance = {};

	information_instance.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	information_instance.pApplicationInfo = &information_application;

	#ifdef USE_VULKAN_VALIDATION_LAYER
		information_instance.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
		information_instance.ppEnabledLayerNames = validationLayers.data();
	#else
		information_instance.enabledLayerCount = 0;
	#endif //USE_VULKAN_VALIDATION_LAYER

	std::vector<const char*> vulkan_extensions = getRequiredExtensions();

	information_instance.enabledExtensionCount = static_cast<uint32_t>(vulkan_extensions.size());
	information_instance.ppEnabledExtensionNames = vulkan_extensions.data();

	VkResult result = vkCreateInstance(
		&information_instance,
		nullptr,
		&instance
	);

	if(result != VK_SUCCESS){
		throw std::runtime_error("Failed to create Vulkan instance.");
	}

	/*
		Available extension enumeration; not necessary for current program, useful reference for future
	*/
	/*
		uint32_t count_extensions = 0;

		vkEnumerateInstanceExtensionProperties(
			nullptr,
			&count_extensions,
			nullptr
		);

		std::vector<VkExtensionProperties> extensions(count_extensions);

		vkEnumerateInstanceExtensionProperties(
			nullptr,
			&count_extensions,
			extensions.data()
		);

		std::cout << "Available extensions:" << std::endl;

		for(const VkExtensionProperties& extension : extensions){
			std::cout << "\t" << extension.extensionName << std::endl;
		}

		std::cout << std::endl;
	*/
}

std::vector<const char*> Application_HelloTriangle::getRequiredExtensions(){
	std::vector<const char*> vulkan_extensions;
	unsigned int count_glfwExtensions = 0;
	const char** _glfwExtensions = glfwGetRequiredInstanceExtensions(&count_glfwExtensions);

	#ifdef USE_VULKAN_VALIDATION_LAYER
		#define OPERATOR_ADD +
		#define COUNT_REQUIRED_DEBUG_EXTENSIONS 1
	#else //THE FOLLOWING DEFINITIONS REMAIN EMPTY
		#define OPERATOR_ADD
		#define COUNT_REQUIRED_DEBUG_EXTENSIONS
	#endif //USE_VULKAN_VALIDATION_LAYER
	
	vulkan_extensions.reserve(count_glfwExtensions OPERATOR_ADD COUNT_REQUIRED_DEBUG_EXTENSIONS);

	#undef OPERATOR_ADD
	#undef COUNT_REQUIRED_DEBUG_EXTENSIONS

	for(
		unsigned int index_glfwExtensions = 0;
		index_glfwExtensions < count_glfwExtensions;
		index_glfwExtensions++
	){
		vulkan_extensions.push_back(_glfwExtensions[index_glfwExtensions]);
	}
	
	#ifdef USE_VULKAN_VALIDATION_LAYER
		//The amount of debug extensions added here should be defined in "COUNT_REQUIRED_DEBUG_EXTENSIONS" above
		vulkan_extensions.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
	#endif //USE_VULKAN_VALIDATION_LAYER

	return vulkan_extensions;
}

void Application_HelloTriangle::createSurface(){
	if(
		glfwCreateWindowSurface(
			instance,
			_window,
			nullptr,
			&surface
		) != VK_SUCCESS
	){
		throw std::runtime_error("Failed to create window surface.");
	}
}

void Application_HelloTriangle::selectSuitablePhysicalDevice(){
	uint32_t count_devices = 0;

	vkEnumeratePhysicalDevices(
		instance,
		&count_devices,
		nullptr
	);

	if(count_devices > 0){
		std::vector<VkPhysicalDevice> physicalDevices(count_devices);

		vkEnumeratePhysicalDevices(
			instance,
			&count_devices,
			physicalDevices.data()
		);

		for(
			unsigned int index_physicalDevices = 0;
			(index_physicalDevices < physicalDevices.size()) && (device_physical == VK_NULL_HANDLE);
			index_physicalDevices++
		){
			const VkPhysicalDevice& physicalDevice_current = physicalDevices.at(index_physicalDevices);

			if(isPhysicalDeviceSuitable(physicalDevice_current)){
				device_physical = physicalDevice_current;
			}
		}

		if(device_physical == VK_NULL_HANDLE){
			throw std::runtime_error("Failed to find a suitable GPU.");
		}
	}else{
		throw std::runtime_error("Failed to find a GPU with Vulkan support.");
	}
}

bool Application_HelloTriangle::isPhysicalDeviceSuitable(VkPhysicalDevice physicalDevice){
	/*
		Physical device property and feature collection; not necessary for current program, useful reference for future
	*/
	/*
		VkPhysicalDeviceProperties physicalDeviceProperties;

		vkGetPhysicalDeviceProperties(
			physicalDevice,
			&physicalDeviceProperties
		);

		VkPhysicalDeviceFeatures physicalDeviceFeatures;

		vkGetPhysicalDeviceFeatures(
			physicalDevice,
			&physicalDeviceFeatures
		);
	*/

	QueueFamily_Indices indices = getQueueFamilies(physicalDevice);
	bool extensionsSupported = checkDeviceExtensionSupport(physicalDevice);

	bool swapChainAdequate = false;

	if(extensionsSupported){
		Details_SwapChain_Support swapChainSupportDetails = querySwapChainSupport(physicalDevice);
		swapChainAdequate = !swapChainSupportDetails.surfaceFormats.empty() && !swapChainSupportDetails.presentationModes.empty();
	}

	return indices.isComplete() && extensionsSupported && swapChainAdequate;
}

bool Application_HelloTriangle::checkDeviceExtensionSupport(VkPhysicalDevice physicalDevice){
	uint32_t count_extensions;

	vkEnumerateDeviceExtensionProperties(
		physicalDevice,
		nullptr,
		&count_extensions,
		nullptr
	);

	std::vector<VkExtensionProperties> availableExtensions(count_extensions);

	vkEnumerateDeviceExtensionProperties(
		physicalDevice,
		nullptr,
		&count_extensions,
		availableExtensions.data()
	);
	
	bool result = true;
	
	for(
		unsigned int index_deviceExtensions = 0;
		(index_deviceExtensions < deviceExtensions.size()) && result;
		index_deviceExtensions++
	){
		const char* _deviceExtension_name = deviceExtensions.at(index_deviceExtensions);
		bool deviceExtensionFound = false;
		
		for(
			unsigned int index_availableExtensions = 0;
			(index_availableExtensions < availableExtensions.size()) && !deviceExtensionFound;
			index_availableExtensions++
		){
			const char* _availableExtension_name = availableExtensions.at(index_availableExtensions).extensionName;

			if(strcmp(_deviceExtension_name, _availableExtension_name) == 0){
				deviceExtensionFound = true;

				#ifdef USE_VULKAN_VALIDATION_LAYER
					std::cout << "Extension: " << _deviceExtension_name << " has been enabled." << std::endl;
				#endif //USE_VULKAN_VALIDATION_LAYER
			}
		}

		result = deviceExtensionFound;
	}

	#ifdef USE_VULKAN_VALIDATION_LAYER
		if(result){
			std::cout << std::endl;
		}
	#endif //USE_VULKAN_VALIDATION_LAYER

	return result;
}

Application_HelloTriangle::QueueFamily_Indices Application_HelloTriangle::getQueueFamilies(VkPhysicalDevice physicalDevice){
	QueueFamily_Indices indices;

	uint32_t count_queueFamilies = 0;

	vkGetPhysicalDeviceQueueFamilyProperties(
		physicalDevice,
		&count_queueFamilies,
		nullptr
	);

	std::vector<VkQueueFamilyProperties> queueFamilies(count_queueFamilies);

	vkGetPhysicalDeviceQueueFamilyProperties(
		physicalDevice,
		&count_queueFamilies,
		queueFamilies.data()
	);

	for(
		unsigned int index_queueFamily = 0;
		(index_queueFamily < queueFamilies.size()) && !indices.isComplete();
		index_queueFamily++
	){
		VkQueueFamilyProperties queueFamily = queueFamilies.at(index_queueFamily);

		if((queueFamily.queueCount > 0) && (queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT)){
			indices.family_graphics = index_queueFamily;
		}

		VkBool32 supportsPresentation = false;

		vkGetPhysicalDeviceSurfaceSupportKHR(
			physicalDevice,
			index_queueFamily,
			surface,
			&supportsPresentation
		);
		
		if((queueFamily.queueCount > 0) && supportsPresentation){
			indices.family_presentation = index_queueFamily;
		}
	}

	return indices;
}

void Application_HelloTriangle::createLogicalDevice(){
	QueueFamily_Indices indices = getQueueFamilies(device_physical);

	std::vector<VkDeviceQueueCreateInfo> informationCollection_queueCreation;
	std::set<int> uniqueQueueFamilies = {
		indices.family_graphics,
		indices.family_presentation
	};
	
	float queuePriority = 1.0f;

	for(int queueFamily : uniqueQueueFamilies){
		VkDeviceQueueCreateInfo information_deviceQueue = {};

		information_deviceQueue.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		information_deviceQueue.queueFamilyIndex = queueFamily;
		information_deviceQueue.queueCount = 1;
		information_deviceQueue.pQueuePriorities = &queuePriority;

		informationCollection_queueCreation.push_back(information_deviceQueue);
	}

	VkPhysicalDeviceFeatures features_device_physical = {};
	VkDeviceCreateInfo information_device_logical = {};

	information_device_logical.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	information_device_logical.queueCreateInfoCount = static_cast<uint32_t>(informationCollection_queueCreation.size());
	information_device_logical.pQueueCreateInfos = informationCollection_queueCreation.data();
	information_device_logical.pEnabledFeatures = &features_device_physical;
	information_device_logical.enabledExtensionCount = static_cast<uint32_t>(deviceExtensions.size());
	information_device_logical.ppEnabledExtensionNames = deviceExtensions.data();

	#ifdef USE_VULKAN_VALIDATION_LAYER
		information_device_logical.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
		information_device_logical.ppEnabledLayerNames = validationLayers.data();
	#else
		information_device_logical.enabledLayerCount = 0;
	#endif //USE_VULKAN_VALIDATION_LAYER

	if(
		vkCreateDevice(
			device_physical,
			&information_device_logical,
			nullptr,
			&device_logical
		) != VK_SUCCESS
	){
		throw std::runtime_error("Failed to create logical device.");
	}

	vkGetDeviceQueue(
		device_logical,
		indices.family_graphics,
		0,
		&queue_graphics
	);
	vkGetDeviceQueue(
		device_logical,
		indices.family_presentation,
		0,
		&queue_presentation
	);
}

Application_HelloTriangle::Details_SwapChain_Support Application_HelloTriangle::querySwapChainSupport(VkPhysicalDevice physicalDevice){
	Details_SwapChain_Support details;

	vkGetPhysicalDeviceSurfaceCapabilitiesKHR(
		physicalDevice,
		surface,
		&details.surfaceCapabilities
	);

	uint32_t count_formats;

	vkGetPhysicalDeviceSurfaceFormatsKHR(
		physicalDevice,
		surface,
		&count_formats,
		nullptr
	);

	if(count_formats != 0){
		details.surfaceFormats.resize(count_formats);

		vkGetPhysicalDeviceSurfaceFormatsKHR(
			physicalDevice,
			surface,
			&count_formats,
			details.surfaceFormats.data()
		);
	}

	uint32_t count_presentationModes;

	vkGetPhysicalDeviceSurfacePresentModesKHR(
		physicalDevice,
		surface,
		&count_presentationModes,
		nullptr
	);

	if(count_presentationModes != 0){
		details.presentationModes.resize(count_presentationModes);

		vkGetPhysicalDeviceSurfacePresentModesKHR(
			physicalDevice,
			surface,
			&count_presentationModes,
			details.presentationModes.data()
		);
	}

	return details;
}

VkSurfaceFormatKHR Application_HelloTriangle::selectSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats){
	VkSurfaceFormatKHR result;

	if((availableFormats.size() == 1) && (availableFormats.at(0).format == VK_FORMAT_UNDEFINED)){
		result = {
			VK_FORMAT_B8G8R8A8_UNORM,
			VK_COLOR_SPACE_SRGB_NONLINEAR_KHR
		};
	}else{
		bool resultFound = false;

		for(
			unsigned int index_availableFormats = 0;
			(index_availableFormats < availableFormats.size()) && !resultFound;
			index_availableFormats++
		){
			VkSurfaceFormatKHR availableFormat_current = availableFormats.at(index_availableFormats);

			if((availableFormat_current.format == VK_FORMAT_B8G8R8A8_UNORM) && (availableFormat_current.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)){
				result = availableFormat_current;
				resultFound = true;
			}
		}

		if(!resultFound){
			result = availableFormats.at(0);
		}
	}

	return result;
}

VkPresentModeKHR Application_HelloTriangle::selectSwapPresentationMode(const std::vector<VkPresentModeKHR> availablePresentationModes){
	VkPresentModeKHR result = VK_PRESENT_MODE_FIFO_KHR;
	bool resultFound = false;

	for(
		unsigned int index_availablePresentationModes = 0;
		(index_availablePresentationModes < availablePresentationModes.size()) && !resultFound;
		index_availablePresentationModes++
	){
		VkPresentModeKHR availablePresentationMode_current = availablePresentationModes.at(index_availablePresentationModes);

		if(availablePresentationMode_current == VK_PRESENT_MODE_MAILBOX_KHR){
			result = availablePresentationMode_current;
			resultFound = true;
		}else if(availablePresentationMode_current == VK_PRESENT_MODE_IMMEDIATE_KHR){
			result = availablePresentationMode_current;
		}
	}

	return result;
}

VkExtent2D Application_HelloTriangle::selectSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities){
	VkExtent2D result;

	if(capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max()){
		result = capabilities.currentExtent;
	}else{
		result = {
			window_width,
			window_height
		};

		result.width = std::max(
			capabilities.minImageExtent.width,
			std::min(
				capabilities.maxImageExtent.width,
				result.width
			)
		);

		result.height = std::max(
			capabilities.minImageExtent.height,
			std::min(
				capabilities.maxImageExtent.height,
				result.height
			)
		);
	}

	return result;
}

void Application_HelloTriangle::createSwapChain(){
	Details_SwapChain_Support swapChainSupportDetails = querySwapChainSupport(device_physical);
	VkSurfaceFormatKHR surfaceFormat = selectSwapSurfaceFormat(swapChainSupportDetails.surfaceFormats);
	VkPresentModeKHR presentationMode = selectSwapPresentationMode(swapChainSupportDetails.presentationModes);
	VkExtent2D extent = selectSwapExtent(swapChainSupportDetails.surfaceCapabilities);

	uint32_t count_images = swapChainSupportDetails.surfaceCapabilities.minImageCount + 1;

	if((swapChainSupportDetails.surfaceCapabilities.maxImageCount > 0) && (count_images > swapChainSupportDetails.surfaceCapabilities.maxImageCount)){
		count_images = swapChainSupportDetails.surfaceCapabilities.maxImageCount;
	}

	VkSwapchainCreateInfoKHR information_swapChain = {};

	information_swapChain.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	information_swapChain.surface = surface;

	information_swapChain.minImageCount = count_images;
	information_swapChain.imageFormat = surfaceFormat.format;
	information_swapChain.imageColorSpace = surfaceFormat.colorSpace;
	information_swapChain.imageExtent = extent;
	information_swapChain.imageArrayLayers = 1;
	information_swapChain.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

	QueueFamily_Indices indices = getQueueFamilies(device_physical);
	uint32_t indices_queueFamily[] = {
		(uint32_t)indices.family_graphics,
		(uint32_t)indices.family_presentation
	};

	if(indices.family_graphics != indices.family_presentation){
		information_swapChain.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
		information_swapChain.queueFamilyIndexCount = 2;
		information_swapChain.pQueueFamilyIndices = indices_queueFamily;
	}else{
		information_swapChain.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
		information_swapChain.queueFamilyIndexCount = 0;
		information_swapChain.pQueueFamilyIndices = nullptr;
	}

	information_swapChain.preTransform = swapChainSupportDetails.surfaceCapabilities.currentTransform;
	information_swapChain.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	information_swapChain.presentMode = presentationMode;
	information_swapChain.clipped = VK_TRUE;
	information_swapChain.oldSwapchain = VK_NULL_HANDLE;

	if(
		vkCreateSwapchainKHR(
			device_logical,
			&information_swapChain,
			nullptr,
			&swapChain
		) != VK_SUCCESS
	){
		throw std::runtime_error("Failed to create swap chain.");
	}

	swapChain_imageFormat = surfaceFormat.format;
	swapChain_extent = extent;

	vkGetSwapchainImagesKHR(
		device_logical,
		swapChain,
		&count_images,
		nullptr
	);

	swapChain_images.resize(count_images);

	vkGetSwapchainImagesKHR(
		device_logical,
		swapChain,
		&count_images,
		swapChain_images.data()
	);
}

void Application_HelloTriangle::createImageViews(){
	swapChain_imageViews.resize(swapChain_images.size());

	for(
		size_t index_swapChain_images = 0;
		index_swapChain_images < swapChain_images.size();
		index_swapChain_images++
	){
		VkImageViewCreateInfo information_image_view = {};

		information_image_view.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		information_image_view.image = swapChain_images.at(index_swapChain_images);
		information_image_view.viewType = VK_IMAGE_VIEW_TYPE_2D;
		information_image_view.format = swapChain_imageFormat;
		information_image_view.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
		information_image_view.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
		information_image_view.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
		information_image_view.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
		information_image_view.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		information_image_view.subresourceRange.baseMipLevel = 0;
		information_image_view.subresourceRange.levelCount = 1;
		information_image_view.subresourceRange.baseArrayLayer = 0;
		information_image_view.subresourceRange.layerCount = 1;

		if(
			vkCreateImageView(
				device_logical,
				&information_image_view,
				nullptr,
				&swapChain_imageViews.at(index_swapChain_images)
			) != VK_SUCCESS
		){
			throw std::runtime_error("Failed to create image views.");
		}
	}
}

VkShaderModule Application_HelloTriangle::createShaderModule(const std::vector<char>& shader_code){
	VkShaderModuleCreateInfo information_shaderModule = {};

	information_shaderModule.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	information_shaderModule.codeSize = shader_code.size();
	information_shaderModule.pCode = reinterpret_cast<const uint32_t*>(shader_code.data());

	VkShaderModule shaderModule;

	if(
		vkCreateShaderModule(
			device_logical,
			&information_shaderModule,
			nullptr,
			&shaderModule
		) != VK_SUCCESS
		){
		throw std::runtime_error("Failed to create shader module.");
	}

	return shaderModule;
}

void Application_HelloTriangle::createRenderPass(){
	VkAttachmentDescription attachment_color_description = {};

	attachment_color_description.format = swapChain_imageFormat;
	attachment_color_description.samples = VK_SAMPLE_COUNT_1_BIT;
	attachment_color_description.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	attachment_color_description.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	attachment_color_description.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	attachment_color_description.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	attachment_color_description.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	attachment_color_description.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

	VkAttachmentReference attachment_color_reference = {};

	attachment_color_reference.attachment = 0;
	attachment_color_reference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	VkSubpassDescription subpass_description = {};

	subpass_description.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	subpass_description.colorAttachmentCount = 1;
	subpass_description.pColorAttachments = &attachment_color_reference;

	VkSubpassDependency subpass_dependency = {};

	subpass_dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
	subpass_dependency.dstSubpass = 0;
	subpass_dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	subpass_dependency.srcAccessMask = 0;
	subpass_dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	subpass_dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

	VkRenderPassCreateInfo information_renderPass = {};

	information_renderPass.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	information_renderPass.attachmentCount = 1;
	information_renderPass.pAttachments = &attachment_color_description;
	information_renderPass.subpassCount = 1;
	information_renderPass.pSubpasses = &subpass_description;
	information_renderPass.dependencyCount = 1;
	information_renderPass.pDependencies = &subpass_dependency;

	if(
		vkCreateRenderPass(
			device_logical,
			&information_renderPass,
			nullptr,
			&renderPass
		) != VK_SUCCESS
		){
		throw std::runtime_error("Failed to create render pass.");
	}
}

void Application_HelloTriangle::createGraphicsPipeline(){
	std::vector<char> Shader_Triangle_Vertex = readFile("Shaders/Shader_Triangle_Vertex.spv");
	std::vector<char> Shader_Triangle_Fragment = readFile("Shaders/Shader_Triangle_Fragment.spv");

	#ifdef USE_VULKAN_VALIDATION_LAYER
		std::cout << "Vertex shader file size: " << Shader_Triangle_Vertex.size() << std::endl;
		std::cout << "Fragment shader file size: " << Shader_Triangle_Fragment.size() << std::endl << std::endl;
	#endif //USE_VULKAN_VALIDATION_LAYER

	VkShaderModule shaderModule_vertex = createShaderModule(Shader_Triangle_Vertex);
	VkShaderModule shaderModule_fragment = createShaderModule(Shader_Triangle_Fragment);

	VkPipelineShaderStageCreateInfo information_pipeline_stage_shader_vertex = {};

	information_pipeline_stage_shader_vertex.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	information_pipeline_stage_shader_vertex.stage = VK_SHADER_STAGE_VERTEX_BIT;
	information_pipeline_stage_shader_vertex.module = shaderModule_vertex;
	information_pipeline_stage_shader_vertex.pName = "main";
	//information_pipelineShaderStageCreation_vertex.pSpecializationInfo //Unused but worth mentioning for future reference. pSpecializationInfo allows you to set the value of shader constants.

	VkPipelineShaderStageCreateInfo information_pipeline_stage_shader_fragment = {};

	information_pipeline_stage_shader_fragment.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	information_pipeline_stage_shader_fragment.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
	information_pipeline_stage_shader_fragment.module = shaderModule_fragment;
	information_pipeline_stage_shader_fragment.pName = "main";

	VkPipelineShaderStageCreateInfo pipelineShaderStages[] = {
		information_pipeline_stage_shader_vertex,
		information_pipeline_stage_shader_fragment
	};

	VkPipelineVertexInputStateCreateInfo information_pipeline_stage_input_vertex = {};

	information_pipeline_stage_input_vertex.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	information_pipeline_stage_input_vertex.vertexBindingDescriptionCount = 0;
	information_pipeline_stage_input_vertex.pVertexBindingDescriptions = nullptr;
	information_pipeline_stage_input_vertex.vertexAttributeDescriptionCount = 0;
	information_pipeline_stage_input_vertex.pVertexAttributeDescriptions = nullptr;

	VkPipelineInputAssemblyStateCreateInfo information_pipeline_state_input_assembly = {};

	information_pipeline_state_input_assembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	information_pipeline_state_input_assembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	information_pipeline_state_input_assembly.primitiveRestartEnable = VK_FALSE;

	VkViewport viewport = {};

	viewport.x = 0.0f;
	viewport.y = 0.0f;
	viewport.width = (float)swapChain_extent.width;
	viewport.height = (float)swapChain_extent.height;
	viewport.minDepth = 0.0f;
	viewport.maxDepth = 1.0f;

	VkRect2D rectangle_scissor = {};
	rectangle_scissor.offset = {0, 0};
	rectangle_scissor.extent = swapChain_extent;
	
	VkPipelineViewportStateCreateInfo information_pipeline_state_viewport = {};

	information_pipeline_state_viewport.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	information_pipeline_state_viewport.viewportCount = 1;
	information_pipeline_state_viewport.pViewports = &viewport;
	information_pipeline_state_viewport.scissorCount = 1;
	information_pipeline_state_viewport.pScissors = &rectangle_scissor;

	VkPipelineRasterizationStateCreateInfo information_pipeline_state_rasterization = {};

	information_pipeline_state_rasterization.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	information_pipeline_state_rasterization.depthClampEnable = VK_FALSE;
	information_pipeline_state_rasterization.rasterizerDiscardEnable = VK_FALSE;
	information_pipeline_state_rasterization.polygonMode = VK_POLYGON_MODE_FILL;
	information_pipeline_state_rasterization.lineWidth = 1.0f;
	information_pipeline_state_rasterization.cullMode = VK_CULL_MODE_BACK_BIT;
	information_pipeline_state_rasterization.frontFace = VK_FRONT_FACE_CLOCKWISE;
	information_pipeline_state_rasterization.depthBiasEnable = VK_FALSE;
	information_pipeline_state_rasterization.depthBiasConstantFactor = 0.0f;
	information_pipeline_state_rasterization.depthBiasClamp = 0.0f;
	information_pipeline_state_rasterization.depthBiasSlopeFactor = 0.0f;

	VkPipelineMultisampleStateCreateInfo information_pipeline_state_multisample = {};
	information_pipeline_state_multisample.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	information_pipeline_state_multisample.sampleShadingEnable = VK_FALSE;
	information_pipeline_state_multisample.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
	information_pipeline_state_multisample.minSampleShading = 1.0f;
	information_pipeline_state_multisample.pSampleMask = nullptr;
	information_pipeline_state_multisample.alphaToCoverageEnable = VK_FALSE;
	information_pipeline_state_multisample.alphaToOneEnable = VK_FALSE;
	
	VkPipelineColorBlendAttachmentState pipelineColorBlendAttachmentState = {};

	pipelineColorBlendAttachmentState.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
	pipelineColorBlendAttachmentState.blendEnable = VK_FALSE;
	pipelineColorBlendAttachmentState.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
	pipelineColorBlendAttachmentState.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
	pipelineColorBlendAttachmentState.colorBlendOp = VK_BLEND_OP_ADD;
	pipelineColorBlendAttachmentState.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
	pipelineColorBlendAttachmentState.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
	pipelineColorBlendAttachmentState.alphaBlendOp = VK_BLEND_OP_ADD;

	//Opacity-based color blending
	/*
		pipelineColorBlendAttachmentState.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
		pipelineColorBlendAttachmentState.blendEnable = VK_TRUE;
		pipelineColorBlendAttachmentState.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
		pipelineColorBlendAttachmentState.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
		pipelineColorBlendAttachmentState.colorBlendOp = VK_BLEND_OP_ADD;
		pipelineColorBlendAttachmentState.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
		pipelineColorBlendAttachmentState.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
		pipelineColorBlendAttachmentState.alphaBlendOp = VK_BLEND_OP_ADD;
	*/

	VkPipelineColorBlendStateCreateInfo information_pipeline_state_colorBlend = {};

	information_pipeline_state_colorBlend.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	information_pipeline_state_colorBlend.logicOpEnable = VK_FALSE; //Turning this on turns OFF all color blend attachment states' blendEnable settings.
	information_pipeline_state_colorBlend.logicOp = VK_LOGIC_OP_COPY;
	information_pipeline_state_colorBlend.attachmentCount = 1;
	information_pipeline_state_colorBlend.pAttachments = &pipelineColorBlendAttachmentState;
	information_pipeline_state_colorBlend.blendConstants[0] = 0.0f;
	information_pipeline_state_colorBlend.blendConstants[1] = 0.0f;
	information_pipeline_state_colorBlend.blendConstants[2] = 0.0f;
	information_pipeline_state_colorBlend.blendConstants[3] = 0.0f;

	//Limited parts of the pipeline state can be modified during runtime
	/*
		VkDynamicState dynamicStates[] = {
			VK_DYNAMIC_STATE_VIEWPORT,
			VK_DYNAMIC_STATE_LINE_WIDTH
		};

		VkPipelineDynamicStateCreateInfo information_pipeline_dynamicState = {};

		information_pipelineDynamicStateCreation.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
		information_pipelineDynamicStateCreation.dynamicStateCount = 2;
		information_pipelineDynamicStateCreation.pDynamicStates = dynamicStates;
	*/

	VkPipelineLayoutCreateInfo information_pipeline_layout = {};

	information_pipeline_layout.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	information_pipeline_layout.setLayoutCount = 0;
	information_pipeline_layout.pSetLayouts = nullptr;
	information_pipeline_layout.pushConstantRangeCount = 0;
	information_pipeline_layout.pPushConstantRanges = 0;

	if(
		vkCreatePipelineLayout(
			device_logical,
			&information_pipeline_layout,
			nullptr,
			&pipeline_layout
		) != VK_SUCCESS
	){
		std::runtime_error("Failed to create pipeline layout.");
	}

	VkGraphicsPipelineCreateInfo information_pipeline_graphics = {};

	information_pipeline_graphics.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
	information_pipeline_graphics.stageCount = 2;
	information_pipeline_graphics.pStages = pipelineShaderStages;
	information_pipeline_graphics.pVertexInputState = &information_pipeline_stage_input_vertex;
	information_pipeline_graphics.pInputAssemblyState = &information_pipeline_state_input_assembly;
	information_pipeline_graphics.pViewportState = &information_pipeline_state_viewport;
	information_pipeline_graphics.pRasterizationState = &information_pipeline_state_rasterization;
	information_pipeline_graphics.pMultisampleState = &information_pipeline_state_multisample;
	information_pipeline_graphics.pDepthStencilState = nullptr;
	information_pipeline_graphics.pColorBlendState = &information_pipeline_state_colorBlend;
	information_pipeline_graphics.pDynamicState = nullptr;
	information_pipeline_graphics.layout = pipeline_layout;
	information_pipeline_graphics.renderPass = renderPass;
	information_pipeline_graphics.subpass = 0;
	information_pipeline_graphics.basePipelineHandle = VK_NULL_HANDLE;
	information_pipeline_graphics.basePipelineIndex = -1;

	if(
		vkCreateGraphicsPipelines(
			device_logical,
			VK_NULL_HANDLE,
			1,
			&information_pipeline_graphics,
			nullptr,
			&pipeline_graphics
		) != VK_SUCCESS
	){
		std::runtime_error("Failed to create graphics pipeline.");
	}

	vkDestroyShaderModule(
		device_logical,
		shaderModule_vertex,
		nullptr
	);
	vkDestroyShaderModule(
		device_logical,
		shaderModule_fragment,
		nullptr
	);
}

void Application_HelloTriangle::createFrameBuffers(){
	swapChain_frameBuffers.resize(swapChain_imageViews.size());

	for(
		size_t index_swapChain_imageViews = 0;
		index_swapChain_imageViews < swapChain_imageViews.size();
		index_swapChain_imageViews++
	){
		VkImageView attachments[] = {
			swapChain_imageViews.at(index_swapChain_imageViews)
		};

		VkFramebufferCreateInfo information_frameBuffer = {};

		information_frameBuffer.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
		information_frameBuffer.renderPass = renderPass;
		information_frameBuffer.attachmentCount = 1;
		information_frameBuffer.pAttachments = attachments;
		information_frameBuffer.width = swapChain_extent.width;
		information_frameBuffer.height = swapChain_extent.height;
		information_frameBuffer.layers = 1;

		if(
			vkCreateFramebuffer(
				device_logical,
				&information_frameBuffer,
				nullptr,
				&swapChain_frameBuffers[index_swapChain_imageViews]
			) != VK_SUCCESS
		){
			throw std::runtime_error("Failed to create framebuffer.");
		}
	}
}

void Application_HelloTriangle::createCommandPool(){
	QueueFamily_Indices indices_queueFamily = getQueueFamilies(device_physical);

	VkCommandPoolCreateInfo information_command_pool = {};

	information_command_pool.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	information_command_pool.queueFamilyIndex = indices_queueFamily.family_graphics;
	information_command_pool.flags = 0;
	//VK_COMMAND_POOL_CREATE_TRANSIENT_BIT: hint that the command buffers are rerecorded with new commands often
	//VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT: allows command buffers to be rerecorded individually, command buffers are reset together without this flag

	if(
		vkCreateCommandPool(
			device_logical,
			&information_command_pool,
			nullptr,
			&command_pool
		) != VK_SUCCESS
	){
		throw std::runtime_error("Failed to create command pool.");
	}
}

void Application_HelloTriangle::createCommandBuffers(){
	command_buffers.resize(swapChain_frameBuffers.size());

	VkCommandBufferAllocateInfo information_command_buffer_allocate = {};

	information_command_buffer_allocate.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	information_command_buffer_allocate.commandPool = command_pool;
	information_command_buffer_allocate.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	information_command_buffer_allocate.commandBufferCount = (uint32_t)command_buffers.size();

	if(
		vkAllocateCommandBuffers(
			device_logical,
			&information_command_buffer_allocate,
			command_buffers.data()
		) != VK_SUCCESS
	){
		throw std::runtime_error("Failed to allocate command buffers.");
	}

	for(
		size_t index_command_buffers = 0;
		index_command_buffers < command_buffers.size();
		index_command_buffers++
	){
		VkCommandBufferBeginInfo information_command_buffer_begin = {};

		information_command_buffer_begin.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		information_command_buffer_begin.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
		information_command_buffer_begin.pInheritanceInfo = nullptr;

		vkBeginCommandBuffer(
			command_buffers.at(index_command_buffers),
			&information_command_buffer_begin
		);

		VkRenderPassBeginInfo information_renderPass_begin = {};

		information_renderPass_begin.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
		information_renderPass_begin.renderPass = renderPass;
		information_renderPass_begin.framebuffer = swapChain_frameBuffers.at(index_command_buffers);
		information_renderPass_begin.renderArea.offset = {0, 0};
		information_renderPass_begin.renderArea.extent = swapChain_extent;
		
		VkClearValue clearValue_color = {
			0.0f,
			0.0f,
			0.0f,
			1.0f
		};

		information_renderPass_begin.clearValueCount = 1;
		information_renderPass_begin.pClearValues = &clearValue_color;

		vkCmdBeginRenderPass(
			command_buffers.at(index_command_buffers),
			&information_renderPass_begin,
			VK_SUBPASS_CONTENTS_INLINE
		);

		vkCmdBindPipeline(
			command_buffers.at(index_command_buffers),
			VK_PIPELINE_BIND_POINT_GRAPHICS,
			pipeline_graphics
		);

		vkCmdDraw(
			command_buffers.at(index_command_buffers),
			3, //Number of vertices
			1, //Number of instances
			0, //The offset into vertex buffer to the first vertex, defines the lowest value of gl_VertexIndex
			0 //Offset for instanced rendering, defines the lowest value of gl_InstanceIndex
		);

		vkCmdEndRenderPass(command_buffers.at(index_command_buffers));

		if(vkEndCommandBuffer(command_buffers.at(index_command_buffers)) != VK_SUCCESS){
			throw std::runtime_error("Failed to record command buffer.");
		}
	}
}

void Application_HelloTriangle::createSemaphores(){
	VkSemaphoreCreateInfo information_semaphore = {};

	information_semaphore.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

	if(
		vkCreateSemaphore(
			device_logical,
			&information_semaphore,
			nullptr,
			&semaphore_imageAvailable
		) != VK_SUCCESS ||
		vkCreateSemaphore(
			device_logical,
			&information_semaphore,
			nullptr,
			&semaphore_renderComplete
		) != VK_SUCCESS
	){
		throw std::runtime_error("Failed to create semaphores.");
	}
}

void Application_HelloTriangle::cleanupSwapChain(){
	for(
		size_t index_swapChain_frameBuffers = 0;
		index_swapChain_frameBuffers < swapChain_frameBuffers.size();
		index_swapChain_frameBuffers++
	){
		vkDestroyFramebuffer(
			device_logical,
			swapChain_frameBuffers.at(index_swapChain_frameBuffers),
			nullptr
		);
	}

	vkFreeCommandBuffers(
		device_logical,
		command_pool,
		static_cast<uint32_t>(command_buffers.size()),
		command_buffers.data()
	); //Manually free the command buffers, we do not need to clean up the command pool when cleaning up the swap chain.
	vkDestroyPipeline(
		device_logical,
		pipeline_graphics,
		nullptr
	);
	vkDestroyPipelineLayout(
		device_logical,
		pipeline_layout,
		nullptr
	);
	vkDestroyRenderPass(
		device_logical,
		renderPass,
		nullptr
	);

	for(
		size_t index_swapChain_imageViews = 0;
		index_swapChain_imageViews < swapChain_imageViews.size();
		index_swapChain_imageViews++
	){
		vkDestroyImageView(
			device_logical,
			swapChain_imageViews.at(index_swapChain_imageViews),
			nullptr
		);
	}

	vkDestroySwapchainKHR(
		device_logical,
		swapChain,
		nullptr
	);
}

void Application_HelloTriangle::recreateSwapChain(){
	vkDeviceWaitIdle(device_logical);

	cleanupSwapChain();

	createSwapChain();
	createImageViews();
	createRenderPass();
	createGraphicsPipeline();
	createFrameBuffers();
	createCommandBuffers();
}

#ifdef USE_VULKAN_VALIDATION_LAYER
	bool Application_HelloTriangle::checkValidationLayerSupport(){
		uint32_t count_layers;

		vkEnumerateInstanceLayerProperties(
			&count_layers,
			nullptr
		);

		std::vector<VkLayerProperties> availableLayers(count_layers);

		vkEnumerateInstanceLayerProperties(
			&count_layers,
			availableLayers.data()
		);

		bool result = true;

		for(
			unsigned int index_validationLayer = 0;
			(index_validationLayer < validationLayers.size()) && result;
			index_validationLayer++
		){
			const char* _validationLayer_name = validationLayers.at(index_validationLayer);
			bool validationLayerFound = false;

			for(
				unsigned int index_availableLayer = 0;
				(index_availableLayer < availableLayers.size()) && !validationLayerFound;
				index_availableLayer++
			){
				const char* _availableLayer_name = availableLayers.at(index_availableLayer).layerName;

				if(strcmp(_validationLayer_name, _availableLayer_name) == 0){
					validationLayerFound = true;

					std::cout << "Layer: " << _validationLayer_name << " has been enabled." << std::endl;
				}
			}

			result = validationLayerFound;
		}

		if(result){
			std::cout << std::endl;
		}

		return result;
	}

	void Application_HelloTriangle::initializeDebugCallback(){
		VkDebugReportCallbackCreateInfoEXT information_debugReport_callback = {};

		information_debugReport_callback.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
		information_debugReport_callback.flags = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT;
		information_debugReport_callback.pfnCallback = callback_debugReport;

		if(
			CreateDebugReportCallbackEXT(
				instance,
				&information_debugReport_callback,
				&handle_callback_debugReport,
				nullptr
			) != VK_SUCCESS
		){
			throw std::runtime_error("Failed to initialize debug callback.");
		}
	}

	VKAPI_ATTR VkBool32 VKAPI_CALL Application_HelloTriangle::callback_debugReport(
		VkDebugReportFlagsEXT flags,
		VkDebugReportObjectTypeEXT objectType,
		uint64_t object,
		size_t location,
		int32_t code,
		const char* _layerPrefix,
		const char* _message,
		void* _customData
	){
		std::cerr << "Validation layer: " << _message << std::endl << std::endl;

		return VK_FALSE;
	}
#endif //USE_VULKAN_VALIDATION_LAYER
