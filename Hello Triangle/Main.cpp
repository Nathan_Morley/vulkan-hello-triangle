#include <iostream>

#include "Application_HelloTriangle.h"

int main(){
	Application_HelloTriangle application(
		(const unsigned short)800, //Window width
		(const unsigned short)600, //Window height
		"Vulkan Hello Triangle" //Application name
	);

	try{
		application.run();
	} catch(const std::runtime_error &exception){
		std::cerr << exception.what() << std::endl;

		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
